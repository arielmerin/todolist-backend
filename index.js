const http = require('http');
const {getTasks, getTaskById, addTask, updateTask, deleteTask, createTaskObj} = require('./todolist-services');
const url = require('url');
const PORT = 8000;

http.createServer(async (request, response) => {
  if(request.method === "GET") {
    //Método GET
    switch(request.url){
      case '/':
          response.end(JSON.stringify({
            message: "todolist server"
          }));
        break;
      case '/tasks': 
        //Regresar la lista de tareas que se encuentra en el archivo todolist.json
        const tasks = await getTasks();
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.end(JSON.stringify(tasks));
        break;
      default:

        const urlObj = url.parse(request.url, true).pathname.split('/');
        const id = urlObj[urlObj.length - 1];
        const resource = urlObj[urlObj.length - 2];
        
        if(resource === "tasks"){
          const task = await getTaskById(id);
          response.end(JSON.stringify(task));
        }

        break;
    }
  }else if(request.method === "POST"){
    //Método POST
    switch(request.url){
      case '/tasks': 
        //Obtenemos los datos que me está enviando el cliente
        let body = "";
        //Evento data -> se dispara cuando un cliente está enviando datos hacía el servidor
        request.on("data", (data) => {
          body += data.toString();
        });

        request.on("end", async() => { //Finalizo la entrega / envio de datos por parte del cliente
          let taskObj = createTaskObj(body);
          await addTask(taskObj);
          response.writeHead(201, {'Content-Type': 'application/json'});
          response.end(JSON.stringify({
            message: "Se ha agregado la tarea en el sistema"
          }))
        });

        break;
      default: 
        break;
    }
  }
}).listen(PORT);